package sudoku;


import java.awt.event.KeyEvent;
import javax.swing.JTextField;

//A modified TextField which only accepts the numerals 1-9 for input, every other input is ignored.
public class SudokuTextField extends JTextField {
  
  private static final long serialVersionUID = 1L;

  public SudokuTextField(int i)
  {
    super(i);
  }
  
  final static String badchars
  = "�{}[]�0-`~!@#$%^&*()_+=\\|\"':;?/>.<, ";
  
  public void processKeyEvent(KeyEvent ev)
  {
    char c = ev.getKeyChar();
    if((Character.isLetter(c) && !ev.isAltDown())
        || badchars.indexOf(c) > -1)
    {
      ev.consume();
      return;
    }
    
    super.processKeyEvent(ev);
  }
}
