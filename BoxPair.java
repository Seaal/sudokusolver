package sudoku;

public class BoxPair
{
  //The first box in the pair
  private final Box box1;
  
  //The second box in the pair
  private final Box box2;
  
  //Constructor
  public BoxPair(Box box1, Box box2)
  {
    this.box1 = box1;
    this.box2 = box2;
  }
  
  //Return the first box in the pair
  public Box getFirstBox()
  {
    return box1;
  }
  
  //Return the second box in the pair
  public Box getSecondBox()
  {
    return box2;
  }
  
  //Returns whether a certain box exists within this pair
  public boolean containsBox(Box box)
  {
    return box1 == box || box2 == box;
  }
  
}
