package sudoku;


public class Checker
{
  private int groupsLeft;
  
  public Checker()
  {
    groupsLeft = 27;
  }
  
  public boolean isSolved()
  {
    return groupsLeft == 0;
  }
  
  public void decreaseGroupsLeft()
  {
    groupsLeft--;
  }
  
  public void reset()
  {
    groupsLeft = 27;
  }
}
