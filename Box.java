package sudoku;

public class Box
{ 
  //Whether or not the value of the box is known or not.
  private boolean solved;
  
  //The value of the box, if not known value is set to 0.
  private int value;
  
  //Whether or not the box could be each of the values 1-9.
  private boolean[] possibleValues;
  
  //The amount of values the box could be.
  private int possibleValuesCount;
  
  //The GUI of the box
  private SudokuTextField gui;
  
  //Constructor, creates a box which contains no value and could be any of the
  //values 1-9.
  public Box()
  {
    solved = false;
    possibleValues = new boolean[10];
    
    possibleValues[0] = false;
    
    for(int i=1;i<10;i++)
    {
      possibleValues[i] = true;
    }
    
    possibleValuesCount = 9;
    value = 0;
  }

  //Returns the value of the box.
  public int getValue()
  {
    return value;
  }

  //Sets the value of the box. If the box already has a value, this method does
  //nothing. If it doesn't sets the value of the box and classes the box as
  //solved. Returns a boolean stating whether the value was successfully set or
  //not.
  public boolean setValue(int whichValue)
  {
    if(solved == true)
    {
      return false;
    }
    else
    {
      value = whichValue;
      solved = true;
      possibleValuesCount = 1; 
      
      for(int i=1;i<10;i++)
      {
        possibleValues[i] = false;
      }
      
      if(gui.getText().length() == 0)
      {
        gui.setText("" + value);
      }
      
      return true;
    }
  }
  
  //Returns whether the box could be a certain value between 1 and 9.
  public boolean getPossibleValue(int whichValue)
  {
    return possibleValues[whichValue];
  }
  
  //Sets the possibility of the box containing a certain value to false. The
  //box can no longer contain that number. If there is only one number that the
  //box can contain, it's value is updated and it is classed as solved. Returns
  //an integer: -1 if it was already impossible to hold that number, 0 if it 
  //can no longer hold that number or a number between 1 and 9 if that is the
  //number the box contains.
  public int setValueImpossible(int whichValue)
  {
    if(possibleValues[whichValue] == false)
    {
      return -1;
    }
    else
    {
      possibleValues[whichValue] = false;
      possibleValuesCount--;
      
      //If there is only one number which the box can contain, update it's
      //value and class it as solved
      if(possibleValuesCount == 1)
      {
        int boxValue = 1;
        
        while(possibleValues[boxValue] == false)
        {
          boxValue++;
        }
        
        setValue(boxValue);
        
        return boxValue;
        
      }
      else
      {
        return 0;
      }
      
    }
    
  }

  //Method which takes two boxes and compares their possible values, calculates
  //if the Boxes are a Pair, a possible triple or a possible quad. Returns 1 if
  //they aren't, 2 if they are a pair, 3 if they are a possible triple and 4 if
  //they are a possible quad.
  public int comparePossibleValues(Box otherBox)
  {
    if(possibleValuesCount != otherBox.possibleValuesCount)
    {
      return 1;
    }
    else if(possibleValuesCount == 2)
    {
      for(int i=0;i<9;i++)
      {
        if(possibleValues[i] != otherBox.possibleValues[i])
        {
          return 1;
        }
      }
      
      return 2;
      
    }
    else
    {
      return 1;
    }
  }
  
  //Returns the number of possible values the box could have.
  public int getPossibleValuesCount()
  {
    return possibleValuesCount;
  }

  //Returns whether the value of the box is known or not.
  public boolean isSolved()
  {
    return solved;
  }

  //Returns the GUI element behind the box
  public SudokuTextField getGui()
  {
    return gui;
  }
  
  //Resets the box allowing it to be used again.
  public void reset()
  {
    
    solved = false;
    
    for(int i=1;i<10;i++)
    {
      possibleValues[i] = true;
    }
    
    possibleValuesCount = 9;
    
    value = 0;
    
    gui.setText("");
  }
  
  //Attaches a TextField GUI element to the box, if it does not already have
  //one
  public void attachGui(SudokuTextField gui)
  {
    if(this.gui == null)
    {
      this.gui = gui;
    }
  }
}
