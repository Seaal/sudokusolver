package sudoku;

import java.util.ArrayList;

public class Group
{
  private final Box[] boxes;
  private boolean solved;
  private ArrayList<BoxPair> pairsList;
  
  public Group(Box[] requiredBoxes)
  {
    boxes = new Box[9];
    
    for(int i=0;i<9;i++)
    {
      boxes[i] = requiredBoxes[i];
    }
    
    pairsList = new ArrayList<BoxPair>();
    
    solved = false;
    
  }
  
  public Box getBox(int whichBox)
  {
    return boxes[whichBox];
  }
  
  public boolean containsBox(Box whichBox)
  {
    for(int i=0;i<9;i++)
    {
      if(boxes[i] == whichBox)
      {
        return true;
      }
    }
    
    return false;
  }
  
  private void checkAllSolved()
  {
    boolean allSolved = true;
    
    for(int i=0;i<9;i++)
    {
      if(!boxes[i].isSolved())
      {
        return;
      }
    }
    
    solved = allSolved;
    
    Solver.getChecker().decreaseGroupsLeft();
  }
  
  public int updatePossibleValues()
  {
    if(solved)
    {
      return -1;
    }
    else
    {
      checkAllSolved();
      
      if(!solved)
      {
        int boxSolved = -1;
        
        loops:
        for(int i=0;i<9;i++)
        {
          if(boxes[i].isSolved())
          {
            for(int j=0;j<9;j++)
            {
              if(j != i && boxes[j].getPossibleValue(boxes[i].getValue()))
              {
                boxSolved = boxes[j].setValueImpossible(boxes[i].getValue());
                if(boxSolved > 0)
                {
                  checkAllSolved();
                  boxSolved = j;
                  break loops;
                }
                else
                {
                  boxSolved = -1;
                }
              }
            }
          }
        }
      
        return boxSolved;
      
      }
      else
      {
        return -1;
      }
    }
  }
  
  public int checkValueOccurences()
  {
    if(solved)
    {
      return -1;
    }
    else
    {
      boolean firstOccurence;
      int whichBox = -1;
      int value = 0;
      
      for(int i=1;i<10;i++)
      {
        firstOccurence = true;
        
        innerLoop:
        for(int j=0;j<9;j++)
        {
          if(!boxes[j].isSolved() && boxes[j].getPossibleValue(i))
          {
            if(firstOccurence)
            {
              firstOccurence = false;
              whichBox = j;
              value = i;
              
            }
            else
            {
              whichBox = -1;
              value = 0;
              break innerLoop;
            }
          }
          else if(boxes[j].getValue() == i)
          {
            whichBox = -1;
            value = 0;
            break innerLoop;
          }
        }
        
        if(whichBox != -1)
        {
          boxes[whichBox].setValue(value);
          checkAllSolved();
          return whichBox;
        }
      }
    }
    
    return -1;
  }
  
  public boolean findPairs()
  {
    boolean hasChanged = false;
    
    if(solved)
    {
      return false;
    }
    else
    {   
      for(int i=0;i<9;i++)
      {
        for(int j=i;j<9;j++)
        {

          if(i != j && !hasPair(boxes[i]) && !hasPair(boxes[j]) && !boxes[i].isSolved() && !boxes[j].isSolved())
          {
            int pair = boxes[i].comparePossibleValues(boxes[j]);
            
            if(pair == 2)
            {
              pairsList.add(new BoxPair(boxes[i],boxes[j]));
              
              hasChanged = true;
              
              for(int k=1;k<10;k++)
              {
                if(boxes[i].getPossibleValue(k))
                {
                  for(int l=0;l<9;l++)
                  {
                    if(l != i && l != j && !boxes[l].isSolved() &&
                       boxes[l].getPossibleValue(k))
                    {
                      boxes[l].setValueImpossible(k);
                    }
                  }
                }
              }
            }
          }
        }
      } 
    }
    
    return hasChanged;
  }
  
  private boolean hasPair(Box box)
  {
    for(BoxPair boxPair : pairsList)
    {
      if(boxPair.containsBox(box))
      {
        return true;
      }
    }
    
    return false;
  }
  
  public void reset(boolean resetBoxes)
  {
  	if(resetBoxes)
  	{	
      for(int i=0;i<9;i++)
      {
        boxes[i].reset();
      }
  	}
    
    solved = false;
  }
  
  public boolean isSolved()
  {
    return solved;
  }
}
