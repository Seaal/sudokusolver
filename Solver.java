package sudoku;

//Controls the solving of the Sudoku

public class Solver
{
  //Each of the groups
  private Group[] groups;
  
  //The checker keeps track of when the Sudoku is solved.
  private static Checker checker;
  
  //Returns the checker
  public static Checker getChecker()
  {
    return checker;
  }
  
  //Constructor Method
  public Solver(Group[] groups)
  {
    this.groups = groups;
    checker = new Checker();
  }
  
  //Method to check which numbers have been input by the user at the start of
  //the solution. Makes their boxes reflect that number and be counted as solved.
  private void initialiseBoxes()
  {
    String tempString;
    
    for(int i=0;i<9;i++)
      for(int j=0;j<9;j++)
      {
        tempString = groups[i].getBox(j).getGui().getText();
        if(tempString.length() == 1)
        {
          groups[i].getBox(j).setValue(Integer.parseInt(tempString));
        }
      }
  }
  
  //Calculates which other 2 groups a box is contained in, when given a group.
  private GroupPair findBoxesGroups(Group currentGroup, Box whichBox)
  {
    Group group1 = null;
    Group group2 = null;
    
    for(Group group: groups)
    {
      if(group.containsBox(whichBox) && group != currentGroup)
      {
        if(group1 == null)
        {
          group1 = group;
        }
        else
        {
          group2 = group;
        }
      }
    }
    
    return new GroupPair(group1,group2);
  }
  
  private boolean updatePossibleValues(Group whichGroup)
  {
    int changedBox = whichGroup.updatePossibleValues();
    
    if(changedBox == -1)
    {
      return false;
    }
    else if(!checker.isSolved())
    {
      GroupPair groupPair = findBoxesGroups(whichGroup, whichGroup.getBox(changedBox));
      updatePossibleValues(groupPair.getGroup1());
      updatePossibleValues(groupPair.getGroup2());
      
      checkValueOccurences(whichGroup);
      checkValueOccurences(groupPair.getGroup1());
      checkValueOccurences(groupPair.getGroup2());
      
      findPairs(whichGroup);
      findPairs(groupPair.getGroup1());
      findPairs(groupPair.getGroup2());

      
      return true;
    }
    else
    {
      return false;
    }
  }
  
  private boolean checkValueOccurences(Group whichGroup)
  {
    int changedBox = whichGroup.checkValueOccurences();
    
    if(changedBox == -1)
    {
      return false;
    }
    else if(!checker.isSolved())
    {
      GroupPair groupPair = findBoxesGroups(whichGroup, whichGroup.getBox(changedBox));
      updatePossibleValues(whichGroup);
      updatePossibleValues(groupPair.getGroup1());
      updatePossibleValues(groupPair.getGroup2());
      
      checkValueOccurences(groupPair.getGroup1());
      checkValueOccurences(groupPair.getGroup2());
      
      findPairs(whichGroup);
      findPairs(groupPair.getGroup1());
      findPairs(groupPair.getGroup2());
      
      return true;
    }
    else
    {
      return false;
    }
  }
  
  private boolean findPairs(Group whichGroup)
  {
    boolean pairFound = whichGroup.findPairs();
    
    if(!pairFound)
    {
      return false;
    }
    else if(!checker.isSolved())
    {
      
      boolean changedBox;
      
      do
      {
        changedBox = updatePossibleValues(whichGroup);
      } while(changedBox == true);
      
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //Method that initiates the solving of a Sudoku
  public Boolean solveSudoku()
  {
    
    //Check which boxes the user has input with numbers, and update them as
    //being solved
    initialiseBoxes();
    
    //The amount of iterations the loop goes through without a box getting
    //solved. If this reaches 27, the Sudoku has tried every method of solving
    //with every group and hence the Sudoku is unsolvable.
    int noChanges = 0;
    
    //Whether the Sudoku has changed or not this iteration
    boolean hasChanged = false;
    
    //The loop control variable
    int i = 0;
    
    //Solving loop
    while(!checker.isSolved() && noChanges < 27)
    {  
      //Checks whether new solved boxes in a group has effected other boxes in
      //that group
      hasChanged = hasChanged || updatePossibleValues(groups[i]);
      
      //Checks whether a box is the only box in a group to possibly contain a 
      //number
      hasChanged = hasChanged || checkValueOccurences(groups[i]);
      
      //Checks whether pairs exist within a group
      hasChanged = hasChanged || findPairs(groups[i]);
      
      //Loop to the next group
      i = (i + 1) % 27;
      
      //If a box is solved then reset the amount of iterations without solving
      //a box
      if(!hasChanged)
      {
        noChanges++;
      }
      else
      {
        noChanges = 0;
        hasChanged = false;
      }
        
    }
    
    //Returns whether the Sudoku is solvable or not
    return checker.isSolved();
  }
}
