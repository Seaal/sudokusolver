package sudoku;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.JButton;

public class SudokuSolver extends JFrame
{
  
  private static final long serialVersionUID = 1L;
  
  //Array that holds each square, column and row, stored as a group object
  private final Group[] boxesGroup;
  
  //Creates each box for a number to be placed into, 81 in total 9x9 grid
  private Box[][] createBoxes()
  {
    Box[][] boxes = new Box[9][9];
    for(int x=0;x<9;x++)
    {
      for(int y=0;y<9;y++)
      {
        boxes[x][y] = new Box();
      }
    }
    
    return boxes;
  }
  
  //Creates a group for each column in the Sudoku
  private void createColumnGroups(Box[][] boxes)
  {
    
    Box[] tempGroup = new Box[9];
    
    for(int x=0;x<9;x++)
    {
      for(int y=0;y<9;y++)
      {
        tempGroup[y] = boxes[x][y];
      }
      
      boxesGroup[x+18] = new Group(tempGroup);
    }
    
  }
  
  //Creates a group for each row in the Sudoku
  private void createRowGroups(Box[][] boxes)
  {
    
    Box[] tempGroup = new Box[9];
    
    for(int y=0;y<9;y++)
    {
      for(int x=0;x<9;x++)
      {
        tempGroup[x] = boxes[x][y];
      }
      
      boxesGroup[y+9] = new Group(tempGroup);
    }
    
  }
  
  //Creates the Sudoku visuals and sets up all boxes and groups needed
  private void createSudokuBoard(JPanel sudokuGridPanel)
  {
    //Make the Sudoku panel a borderless 3x3 grid of dimensions 486x486
    sudokuGridPanel.setLayout(new GridLayout(3,3,0,0));
    sudokuGridPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
    sudokuGridPanel.setPreferredSize(new Dimension(486,486));
    
    //Create the boxes and store them within a temporary array
    Box[][] tempBoxes = createBoxes();
    
    //Create an array of panels one for each square in the Sudoku Panel grid
    JPanel[] cellPanel = new JPanel[9];
    
    //A temporary array to store boxes together so they can be grouped
    Box[] tempGroup = new Box[9];
    
    //For each square in the sudoku Panel Grid
    for(int i=0;i<9;i++)
    {
      //Create another 3x3 Grid inside it, totalling 81 squares
      cellPanel[i] = new JPanel();
      sudokuGridPanel.add(cellPanel[i]);
      cellPanel[i].setLayout(new GridLayout(3,0));
      
      //Give each Panel a border, outlining the 9 different squares
      cellPanel[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));

      //For each square in the smaller 3x3
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          //Create a blank textfield in each, representing each Sudoku square
          SudokuTextField tempField = new SudokuTextField(2);
          tempField.setHorizontalAlignment(JTextField.CENTER);
          tempField.setPreferredSize(new Dimension(54,54));    
          tempField.setFont(new Font("SansSerif", Font.BOLD, 24));
          tempField.setText("");
          tempField.setDocument(new JTextFieldLimit(1));
          
          //Attach each textfield to the relevant box
          tempBoxes[k+(i%3)*3][j+(i/3)*3].attachGui(tempField);
          
          //Add the textfield to the Panel
          cellPanel[i].add(tempField);
          
          //Group each box into the square it belongs to
          tempGroup[k+3*j] = tempBoxes[k+(i%3)*3][j+(i/3)*3];
        } 
      }
      
      //Add each group to the array
      boxesGroup[i] = new Group(tempGroup);
    }
    
    //Form the groups for the rows and columns
    createRowGroups(tempBoxes);
    createColumnGroups(tempBoxes);
  }
  
  //Create the options panel
  private void createOptions(JPanel optionsPanel, JTextField info, JButton startButton)
  {
    optionsPanel.setLayout(new GridLayout(0,1));
    
    info.setEnabled(false);
    info.setHorizontalAlignment(JTextField.CENTER);
    
    SudokuListener listener = new SudokuListener(boxesGroup, info, startButton);
    startButton.addActionListener(listener);
    
    optionsPanel.add(info);
    optionsPanel.add(startButton);
    
  }
  
  public SudokuSolver()
  {
    //The columns, rows and squares of the Sudoku
    boxesGroup = new Group[27];
    
    //Create the basic GUI elements
    setTitle("Sudoku Solver");
    
    Container contents = getContentPane();
    contents.setLayout(new BorderLayout());
    
    JPanel sudokuGridPanel = new JPanel();
    contents.add(sudokuGridPanel,BorderLayout.CENTER);
    
    JPanel optionsPanel = new JPanel();
    contents.add(optionsPanel,BorderLayout.SOUTH);
    
    //Create the Sudoku Board
    createSudokuBoard(sudokuGridPanel);
    
    //Create the options
    JTextField info = new JTextField();
    JButton startButton = new JButton("Solve Sudoku!");
    createOptions(optionsPanel, info, startButton);
    
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
    
  }
  
	public static void main(String[] args)
	{
	  //Create an instance of the Sudoku Solver Gui and show the user it
	  SudokuSolver ss = new SudokuSolver();
	  ss.setVisible(true);
	}

}
