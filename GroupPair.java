package sudoku;


public class GroupPair
{
  private final Group group1;
  private final Group group2;
  
  public GroupPair(Group group1, Group group2)
  {
    this.group1 = group1;
    this.group2 = group2;
  }
  
  public Group getGroup1()
  {
    return group1;
  }
  
  public Group getGroup2()
  {
    return group2;
  }
}
