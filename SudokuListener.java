package sudoku;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;


public class SudokuListener implements ActionListener
{
  //Array that holds each square, column and row, stored as a group object
  private final Group[] boxesGroup;
  
  private boolean needsToReset;
  private final JTextField info;
  private final JButton startButton;
  
  public SudokuListener(Group[] boxesGroup, JTextField info, JButton startButton)
  {
    this.boxesGroup = boxesGroup;
    this.info = info;
    this.startButton = startButton;
    needsToReset = false;
  }
  
//Sets each Sudoku box to allow or disallow input
  private void setEnabledSudokuTextFields(boolean enable)
  {
    for(int i=0;i<9;i++)
    {
      for(int j=0;j<9;j++)
      {
        boxesGroup[i].getBox(j).getGui().setEnabled(enable);
      }
    }
  }
  
  public void actionPerformed(ActionEvent event)
  {
    if(needsToReset)
    {
      for(int i=0;i<27;i++)
      {
        boxesGroup[i].reset(i<9 ? true : false);
      }
      
      startButton.setText("Solve Sudoku!");
      
      info.setText("");
      
      setEnabledSudokuTextFields(true);
      
      needsToReset = false;
      
      Solver.getChecker().reset();
    }
    else
    {
      setEnabledSudokuTextFields(false);
      
      startButton.setEnabled(false);
      
      info.setText("Solving Sudoku...");
      
      Solver solver = new Solver(boxesGroup);
      
      boolean solved = solver.solveSudoku();
      
      if(solved)
      {
        info.setText("Sudoku solved!");
      }
      else
      {
        info.setText("Sudoku was unable to be solved.");
      }
      
      startButton.setText("Reset");
      startButton.setEnabled(true);
      
      needsToReset = true;
    }
  }
}
